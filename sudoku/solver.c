#include<stdio.h>
#include<time.h>
int mark[9][9]={{0}};
int check(int sud[9][9],int r,int c,int num)  
{
	int i,j,li,lj;
	for(i=0;i<9;i++)
		if(i!=r && sud[i][c]==num)
			return 0;
	for(j=0;j<9;j++)
		if(j!=c && sud[r][j]==num)
			return 0;
	li=r/3;
	lj=c/3;
	for(i=3*li;i<3*(li+1);i++)
		for(j=3*lj;j<3*(lj+1);j++)
		{
			if(i!=r && j!=c && sud[i][j]==num)
				return 0;
		}
	return 1;
}
int solve(int sud[9][9],int r,int c)
{
	if(mark[r][c]==0)
	{
		for(++sud[r][c];sud[r][c]<=9;sud[r][c]++)
		{
			if(check(sud,r,c,sud[r][c])!=0)
			{
				if(r==8 && c==8)
				{
					return 1;
				}
				else if(c==8)
				{
					if(solve(sud,r+1,0)==1)
						return 1;
				}
				else
				{
					if(solve(sud,r,c+1)==1)
						return 1;
				}
			}
		}
		sud[r][c]=0;
	}
	else
	{
		if(r==8 && c==8)
		{
			return 1;
		}
		else if(c==8)
		{
			if(solve(sud,r+1,0)==1)
				return 1;
		}
		else
		{
			if(solve(sud,r,c+1)==1)
				return 1;
		}
	}
	return 0;
}
void print(int sud[9][9])
{
	int i,j;
	for(i=0;i<9;i++)
	{
		if(i%3==0)
			printf(" -------------------------------------\n");
		for(j=0;j<9;j++)
		{
			if(j%3==0)
				printf(" | ");
			printf(" %d ",sud[i][j]);
		}
		printf(" | \n");
	}
	printf(" -------------------------------------\n");
}
int main()
{
	int i,j,sud[9][9];
	printf("Enter the sudoku : (Put 0 if blank)\n");
	for(i=0;i<9;i++)
                for(j=0;j<9;j++)
                {
                        scanf("%d",&sud[i][j]);
                        if(sud[i][j]!=0)
                                mark[i][j]=1;
                }
        printf("\nThe entered sudoku is :\n");
        print(sud);
        if(solve(sud,0,0)==1)
	{
        	printf("\n\nThe Sudoku solution is:\n");
        	print(sud);
	}
	else
        	printf("\n\nThe Sudoku is unsolvable.\n");
        return 0;
}
