#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int mark[9][9]={{0}};
int check(int sud[9][9],int r,int c,int num)  
{
	int i,j,li,lj;
	for(i=0;i<9;i++)
		if(i!=r && sud[i][c]==num)
			return 0;
	for(j=0;j<9;j++)
		if(j!=c && sud[r][j]==num)
			return 0;
	li=r/3;
	lj=c/3;
	for(i=3*li;i<3*(li+1);i++)
		for(j=3*lj;j<3*(lj+1);j++)
		{
			if(i!=r && j!=c && sud[i][j]==num)
				return 0;
		}
	return 1;
}
int solve(int sud[9][9],int r,int c)
{
	if(mark[r][c]==0)
	{
		for(++sud[r][c];sud[r][c]<=9;sud[r][c]++)
		{
			if(check(sud,r,c,sud[r][c])!=0)
			{
				if(r==8 && c==8)
				{
					return 1;
				}
				else if(c==8)
				{
					if(solve(sud,r+1,0)==1)
						return 1;
				}
				else
				{
					if(solve(sud,r,c+1)==1)
						return 1;
				}
			}
		}
		sud[r][c]=0;
	}
	else
	{
		if(r==8 && c==8)
		{
			return 1;
		}
		else if(c==8)
		{
			if(solve(sud,r+1,0)==1)
				return 1;
		}
		else
		{
			if(solve(sud,r,c+1)==1)
				return 1;
		}
	}
	return 0;
}
void print(int sud[9][9])
{
	int i,j;
	for(i=0;i<9;i++)
	{
		if(i%3==0)
			printf(" -------------------------------------\n");
		for(j=0;j<9;j++)
		{
			if(j%3==0)
				printf(" | ");
			printf(" %d ",sud[i][j]);
		}
		printf(" | \n");
	}
	printf(" -------------------------------------\n");
}
int main()
{
	int i,j,sud[9][9]={{0}},row,col,num;
	srand(time(NULL));
	sud[0][0]=rand()%9+1;
	mark[0][0]=1;
	for(i=0;i<7;i++)
	{
		row=rand()%9;
		col=rand()%9;
		num=rand()%9+1;
		//printf("%d %d %d\n",row,col,num);
		while(check(sud,row,col,num)==0)
			num=rand()%9+1;
		sud[row][col]=num;
		mark[row][col]=1;
	}
	printf("\nThe generated sudoku is :\n");
	print(sud);
	solve(sud,0,0);
	printf("\n\nThe Sudoku solution is:\n");
	print(sud);
	for(i=0;i<9;i++)
		for(j=0;j<9;j++)
			mark[i][j]=0;
	for(i=0;i<9;i++)
	{
		for(j=0;j<4;)
		{
			num=rand()%9;
			if(mark[i][num]==0)
			{
				mark[i][num]=1;
				j++;
			}
		}
	}
	printf("\n\nThe Sudoku question is:\n");
	for(i=0;i<9;i++)
	{
		if(i%3==0)
			printf(" -------------------------------------\n");
		for(j=0;j<9;j++)
		{
			if(j%3==0)
				printf(" | ");
			if(mark[i][j]==1)
				printf(" %d ",sud[i][j]);
			else
				printf(" 0 ");
		}
		printf(" | \n");
	}
	printf(" -------------------------------------\n");
	return 0;
}
