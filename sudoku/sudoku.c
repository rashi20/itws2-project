#include<stdio.h>
#include<stdlib.h>
int mark[9][9]={{0}};
int check(int sud[9][9],int r,int c,int num)
{
	int i,j,li,lj;
	for(i=0;i<r;i++)
		if(sud[i][c]==num)
			return 0;
	for(j=0;j<9;j++)
		if(j!=c && sud[r][j]==num)
			return 0;
	li=r/3;
	lj=c/3;
	for(i=3*li;i<3*(li+1);i++)
		for(j=3*lj;j<3*(lj+1);j++)
			if(i!=r && j!=c && sud[i][j]==num)
				return 0;
	return 1;
}
void end()
{
	printf("Program is exiting!! You gave me an unsolvable sudoku :( \n");
	exit(0);
}
int checkone(int sud[10][10])
{
	int i,j,n,flag;
	for(i=0;i<9;i++)
		for(j=0;j<9;j++)
		{
			if(!mark[i][j])
			{
				flag=0;
				for(n=1;n<=9&&!flag;n++)
				{
					if(check(sud,i,j,n))
						flag=1;
				}
				if(!flag)
					return 0;
			}
		}
	return 1;
}					
void solve(int sud[10][10],int r,int c)
{
	int i,j,n,flag;
	if(!mark[r][c])
	{
		flag=0;
		for(n=sud[r][c]+1;n<=9;n++)
		{
			if(check(sud,r,c,n))
			{
				sud[r][c]=n;
				flag=1;
				break;
			}
		}
		if(!flag)
		{
			sud[r][c]=0;
			for(i=r;i>=0;i--)
			{
				if(i==r && c)
					j=c-1;
				else if(i==r && !c)
				{
					i--;
					j=8;
				}
				else
					j=8;
				for(;j>=0;j--)
				{
					if(!mark[i][j])
					{
						flag=1;
						break;
					}
				}
				if(flag)
					break;
			}
			if(flag)
				solve(sud,i,j);
			else
				end();
			return ;
		}
	}
	if(r==8 && c==8)
		return ;
	else if(c==8)
		solve(sud,r+1,0);
	else
		solve(sud,r,c+1);
}
void print(int sud[10][10])
{
	int i,j;
	for(i=0;i<9;i++)
	{
		if(i%3==0)
			printf(" -------------------------------------\n");
		for(j=0;j<9;j++)
		{
			if(j%3==0)
				printf(" | ");
			printf(" %d ",sud[i][j]);
		}
		printf(" | \n");
	}
	printf(" -------------------------------------\n");

}
int validate(int sud[10][10])
{
	int i,j;
	for(i=0;i<9;i++)
		for(j=0;j<9;j++)
		{
			if(sud[i][j])
			{
				if(!check(sud,i,j,sud[i][j]))
					return 0;
			}
		}
	return 1;
}
int main()
{
	int i,j,sud[9][9];
	printf("Enter the sudoku : (Put 0 if blank)\n");
	for(i=0;i<9;i++)
		for(j=0;j<9;j++)
		{
			scanf("%d",&sud[i][j]);
			if(sud[i][j]!=0)
				mark[i][j]=1;
		}
	printf("\nThe entered sudoku is :\n");
	print(sud);
	if(validate(sud)==0||!checkone(sud)!=0)
		end();
	solve(sud,0,0);
	printf("\n\nThe Sudoku solution is:\n");
	print(sud);
	return 0;
}
