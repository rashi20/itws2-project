# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()
auth.settings.extra_fields['auth_user']= [
Field('scoreinlevi1','integer',default=0,readable=False,writable=False),
Field('scoreinlevi2','integer',default=0,readable=False,writable=False),
Field('scoreinlevi3','integer',default=0,readable=False,writable=False),
Field('scoreinlevg1','integer',default=0,readable=False,writable=False),
Field('scoreinlevg2','integer',default=0,readable=False,writable=False),
Field('scoreinlevg3','integer',default=0,readable=False,writable=False),
Field('scoreinlevt1','integer',default=0,readable=False,writable=False),
Field('scoreinlevt2','integer',default=0,readable=False,writable=False),
Field('scoreinlevt3','integer',default=0,readable=False,writable=False),
Field('complevi1','integer',default=0,readable=False,writable=False),
Field('complevi2','integer',default=0,readable=False,writable=False),
Field('complevg1','integer',default=0,readable=False,writable=False),
Field('complevg2','integer',default=0,readable=False,writable=False),
Field('complevt1','integer',default=0,readable=False,writable=False),
Field('complevt2','integer',default=0,readable=False,writable=False),
Field('currques','integer',default=1,readable=False,writable=False),
Field('timeinlevi1','integer',default=0,readable=False,writable=False),
Field('timeinlevi2','integer',default=0,readable=False,writable=False),
Field('timeinlevi3','integer',default=0,readable=False,writable=False),
Field('timeinlevg1','integer',default=0,readable=False,writable=False),
Field('timeinlevg2','integer',default=0,readable=False,writable=False),
Field('timeinlevg3','integer',default=0,readable=False,writable=False),
]
## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
auth.settings.allow_basic_login = True
db.define_table('questionslevi1',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'))
db.define_table('questionslevi2',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'))
db.define_table('questionslevi3',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'))
db.define_table('questionslevg1',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'))
db.define_table('questionslevg2',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'))
db.define_table('questionslevg3',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'))
db.define_table('questionslevt1',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'),Field('timage','upload',default=0))
db.define_table('questionslevt2',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'),Field('timage','upload',default=0))
db.define_table('questionslevt3',Field('question','string'),Field('op1','string'),Field('op2','string'),Field('op3','string'),Field('op4','string'),Field('rightans','string'),Field('quesno','integer'),Field('timage','upload',default=0))
db.define_table('ranklistlevg',Field('authid','integer',unique=True),Field('firstname','string'),Field('lastname','string'),Field('scoreinlevg1','integer',default=0),Field('scoreinlevg2','integer',default=0),Field('scoreinlevg3','integer',default=0),Field('timeinlevg1','integer',default=0),Field('timeinlevg2','integer',default=0),Field('timeinlevg3','integer',default=0),Field('totalscore',compute=lambda r: r['scoreinlevg1']+r['scoreinlevg2']+r['scoreinlevg3']),Field('totaltime',compute=lambda r: r['timeinlevg1']+r['timeinlevg2']+r['timeinlevg3']))
db.define_table('ranklistlevi',Field('authid','integer',unique=True),Field('firstname','string'),Field('lastname','string'),Field('scoreinlevi1','integer',default=0),Field('scoreinlevi2','integer',default=0),Field('scoreinlevi3','integer',default=0),Field('timeinlevi1','integer',default=0),Field('timeinlevi2','integer',default=0),Field('timeinlevi3','integer',default=0),Field('totalscore',compute=lambda r: r['scoreinlevi1']+r['scoreinlevi2']+r['scoreinlevi3']),Field('totaltime',compute=lambda r: r['timeinlevi1']+r['timeinlevi2']+r['timeinlevi3']))
db.define_table('ranklistlevt',Field('authid','integer',unique=True),Field('firstname','string'),Field('lastname','string'),Field('scoreinlevt1','integer',default=0),Field('scoreinlevt2','integer',default=0),Field('scoreinlevt3','integer',default=0),Field('totalscore',compute=lambda r: r['scoreinlevt1']+r['scoreinlevt2']+r['scoreinlevt3']))
