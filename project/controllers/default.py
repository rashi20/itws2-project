# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    auth.basic()
    if auth.user:
	    response.flash='Hello %(first_name)s' % auth.user
    response.logo="Quizzaria"
    return dict(message=T('Get ready for some action'))

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    response.logo="Quizzaria"    
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())

@auth.requires_login()
def start1():
     roll=db(db.auth_user.id==auth.user.id).select()
     for i in roll:
		level1=i.complevi1
		level2=i.complevi2
     return dict(level1=level1,level2=level2)	
@auth.requires_login()
def start2():
     roll=db(db.auth_user.id==auth.user.id).select()
     for i in roll:
		level1=i.complevg1
		level2=i.complevg2
     return dict(level1=level1,level2=level2)		
@auth.requires_login()
def start3():
     db(db.auth_user.id==auth.user.id).update(currques=1)
     roll=db(db.auth_user.id==auth.user.id).select()
     for i in roll:
		level1=i.complevt1
		level2=i.complevt2
     return dict(level1=level1,level2=level2)	

def home():
    redirect(URL('index'))

def levi1():
	db(db.auth_user.id==auth.user.id).update(scoreinlevi1=0)
	db(db.auth_user.id==auth.user.id).update(timeinlevi1=0)
	db(db.auth_user.id==auth.user.id).update(currques=1)
	rows=db(db.questionslevi1.quesno==1).select()
	for row in rows:
		q=row.question
		a=row.op1
		b=row.op2
		c=row.op3
		d=row.op4
	return dict(question=q,op1=a,op2=b,op3=c,op4=d)

def levg1():
	db(db.auth_user.id==auth.user.id).update(scoreinlevg1=0)
	db(db.auth_user.id==auth.user.id).update(timeinlevg1=0)
	db(db.auth_user.id==auth.user.id).update(currques=1)
	rows=db(db.questionslevg1.quesno==1).select()
	for row in rows:
		q=row.question
		a=row.op1
		b=row.op2
		c=row.op3
		d=row.op4
	return dict(question=q,op1=a,op2=b,op3=c,op4=d)

def levt1():
	roll=db(db.auth_user.id==auth.user.id).select()
    	for i in roll:
		currquesi=i.currques
	if currquesi==1:
		db(db.auth_user.id==auth.user.id).update(scoreinlevt1=0)
	rows=db(db.questionslevt1.quesno==currquesi).select()
	for row in rows:
		q=row.question
		a=row.op1
		b=row.op2
		c=row.op3
		d=row.op4
		tim=row.timage
	db(db.auth_user.id==auth.user.id).update(currques=currquesi+1)
	return dict(question=q,op1=a,op2=b,op3=c,op4=d,timg=tim)


def levt2():
      	roll=db(db.auth_user.id==auth.user.id).select()
	for row in roll:
		yes=row.complevt1
	if yes==1:
		roll=db(db.auth_user.id==auth.user.id).select()
    		for i in roll:
			currquesi=i.currques
		if currquesi==1:
			db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
		rows=db(db.questionslevt2.quesno==currquesi).select()
		for row in rows:
			q=row.question
			a=row.op1
			b=row.op2
			c=row.op3
			d=row.op4
			tim=row.timage
		db(db.auth_user.id==auth.user.id).update(currques=currquesi+1)
		return dict(question=q,op1=a,op2=b,op3=c,op4=d,timg=tim)
	else:
		return "Sorry, you haven't completed level 1" 

def levt3():
      	roll=db(db.auth_user.id==auth.user.id).select()
	for row in roll:
		yes=row.complevt2
	if yes==1:
		roll=db(db.auth_user.id==auth.user.id).select()
    		for i in roll:
			currquesi=i.currques
		if currquesi==1:
			db(db.auth_user.id==auth.user.id).update(scoreinlevt3=0)
		rows=db(db.questionslevt3.quesno==currquesi).select()
		for row in rows:
			q=row.question
			a=row.op1
			b=row.op2
			c=row.op3
			d=row.op4
			tim=row.timage
		db(db.auth_user.id==auth.user.id).update(currques=currquesi+1)
		return dict(question=q,op1=a,op2=b,op3=c,op4=d,timg=tim)
	else:
		return "Sorry, you haven't completed previous level" 

def nexti1():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevi1
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevi1.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
	if ques!=10:
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))

def storeg1():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	k=str(request.vars.abc)
	k=int(k)
	db(db.auth_user.id==auth.user.id).update(timeinlevg1=k)
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		firstnamea=row.first_name
		lastnamea=row.last_name
		scoreinlevg1a=row.scoreinlevg1
		timeinlevg1a=row.timeinlevg1
	ret=db.ranklistlevg.validate_and_insert(authid=refid,firstname=firstnamea,lastname=lastnamea,scoreinlevg1=scoreinlevg1a,timeinlevg1=timeinlevg1a)
	r=db(db.ranklistlevg.authid==refid).select()
	for i in r:
		db(db.ranklistlevg.authid==refid).validate_and_update(scoreinlevg1=scoreinlevg1a,scoreinlevg2=i.scoreinlevg2,scoreinlevg3=i.scoreinlevg3,timeinlevg2=i.timeinlevg2,timeinlevg1=timeinlevg1a,timeinlevg3=i.timeinlevg3)
	return "take_end();"
def nextg1():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevg1
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevg1.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
	if ques!=10:
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))

def nextt1():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevt1
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevt1.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
		timg2=row.timage
		timg="<img src=\"{{=URL(r=request, c='default', f='download', args=+"+timg2+")}}\" alt=\"blah\" /><br/>" 
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#timg').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(timg))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#timg').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(timg))

def echoi1():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevi1
		ques=i.currques
	rows=db(db.questionslevi1.quesno==ques).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevi1=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==10 and (mark+1)>=8:
			db(db.auth_user.id==auth.user.id).update(complevi1=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==10 and (mark+1)<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==10 and mark>=8:
			db(db.auth_user.id==auth.user.id).update(complevi1=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==10 and mark<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
	
def echog1():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevg1
		ques=i.currques
	rows=db(db.questionslevg1.quesno==ques).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevg1=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==10 and (mark+1)>=8:
			db(db.auth_user.id==auth.user.id).update(complevg1=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==10 and (mark+1)<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==10 and mark>=8:
			db(db.auth_user.id==auth.user.id).update(complevg1=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==10 and mark<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))

def echot2():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevt2
		ques=i.currques
	rows=db(db.questionslevt2.quesno==(ques-1)).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevt2=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==3 and (mark+1)>=2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			db(db.auth_user.id==auth.user.id).update(complevt2=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==3 and (mark+1)<2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==3 and mark>=2:
			db(db.auth_user.id==auth.user.id).update(complevt2=1)
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==3 and mark<2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))

def echot3():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevt3
		ques=i.currques
	rows=db(db.questionslevt3.quesno==(ques-1)).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevt3=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==3 and (mark+1)>=2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==3 and (mark+1)<2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==3 and mark>=2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==3 and mark<2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			#db(db.auth_user.id==auth.user.id).update(scoreinlevt2=0)
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))

def echot1():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevt1
		ques=i.currques
	rows=db(db.questionslevt1.quesno==(ques-1)).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevt1=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==3 and (mark+1)>=2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			db(db.auth_user.id==auth.user.id).update(complevt1=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==3 and (mark+1)<2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==3 and mark>=2:
			db(db.auth_user.id==auth.user.id).update(complevt1=1)
			db(db.auth_user.id==auth.user.id).update(currques=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==3 and mark<2:
			db(db.auth_user.id==auth.user.id).update(currques=1)
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))

def storei1():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	k=str(request.vars.abc)
	k=int(k)
	db(db.auth_user.id==auth.user.id).update(timeinlevi1=k)
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		firstnamea=row.first_name
		lastnamea=row.last_name
		scoreinlevi1a=row.scoreinlevi1
		timeinlevi1a=row.timeinlevi1
	ret=db.ranklistlevi.validate_and_insert(authid=refid,firstname=firstnamea,lastname=lastnamea,scoreinlevi1=scoreinlevi1a,timeinlevi1=timeinlevi1a)
	r=db(db.ranklistlevi.authid==refid).select()
	for i in r:
		db(db.ranklistlevi.authid==refid).validate_and_update(scoreinlevi1=scoreinlevi1a,scoreinlevi2=i.scoreinlevi2,scoreinlevi3=i.scoreinlevi3,timeinlevi2=i.timeinlevi2,timeinlevi1=timeinlevi1a,timeinlevi3=i.timeinlevi3)
	return "take_end();"
def storei2():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	k=str(request.vars.abc)
	k=int(k)
	db(db.auth_user.id==auth.user.id).update(timeinlevi2=k)
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		scoreinlevi1a=row.scoreinlevi2
		timeinlevi1a=row.timeinlevi2
	r=db(db.ranklistlevi.authid==refid).select()
	for i in r:
		db(db.ranklistlevi.authid==refid).validate_and_update(scoreinlevi2=scoreinlevi1a,scoreinlevi1=i.scoreinlevi1,scoreinlevi3=i.scoreinlevi3,timeinlevi1=i.timeinlevi1,timeinlevi2=timeinlevi1a,timeinlevi3=i.timeinlevi3)
	return "take_end();"

	
def storet1():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		firstnamea=row.first_name
		lastnamea=row.last_name
		scoreinlevt1a=row.scoreinlevt1
	ret=db.ranklistlevt.validate_and_insert(authid=refid,firstname=firstnamea,lastname=lastnamea,scoreinlevt1=scoreinlevt1a)
	r=db(db.ranklistlevt.authid==refid).select()
	for i in r:
		db(db.ranklistlevt.authid==refid).validate_and_update(scoreinlevt1=scoreinlevt1a,scoreinlevt2=i.scoreinlevt2,scoreinlevt3=i.scoreinlevt3)
	return "take_end();"	

def storet2():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		scoreinlevi1a=row.scoreinlevt2
	r=db(db.ranklistlevt.authid==refid).select()
	for i in r:
		db(db.ranklistlevt.authid==refid).validate_and_update(scoreinlevt2=scoreinlevi1a,scoreinlevt1=i.scoreinlevt1,scoreinlevt3=i.scoreinlevt3)
	return "take_end();"	
def storet3():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		scoreinlevi1a=row.scoreinlevt3
	r=db(db.ranklistlevt.authid==refid).select()
	for i in r:
		db(db.ranklistlevt.authid==refid).validate_and_update(scoreinlevt3=scoreinlevi1a,scoreinlevt1=i.scoreinlevt1,scoreinlevt2=i.scoreinlevt2)
	return "take_end();"	

def levi2():
  	roll=db(db.auth_user.id==auth.user.id).select()
	for row in roll:
		yes=row.complevi1
	if yes==1:
		db(db.auth_user.id==auth.user.id).update(scoreinlevi2=0)
		db(db.auth_user.id==auth.user.id).update(timeinlevi2=0)
		db(db.auth_user.id==auth.user.id).update(currques=1)
		rows=db(db.questionslevi2.quesno==1).select()
		for row in rows:
			q=row.question
			a=row.op1
			b=row.op2
			c=row.op3
			d=row.op4
		return dict(question=q,op1=a,op2=b,op3=c,op4=d)
	else:
		return "Sorry, you haven't completed level 1"

def echoi2():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevi2
		ques=i.currques
	rows=db(db.questionslevi2.quesno==ques).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevi2=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==10 and (mark+1)>=8:
			db(db.auth_user.id==auth.user.id).update(complevi2=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==10 and (mark+1)<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==10 and mark>=8:
			db(db.auth_user.id==auth.user.id).update(complevi2=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==10 and mark<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
	
def nexti2():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevi2
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevi2.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
	if ques!=10:
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))	

def storei2():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	a=""
	db(db.auth_user.id==auth.user.id).update(timeinlevi2=request.vars.abc)
	return "take_end();"	

def levi3():
	roll=db(db.auth_user.id==auth.user.id).select()
	for row in roll:
		yes=row.complevi2
	if yes==1:
		db(db.auth_user.id==auth.user.id).update(scoreinlevi3=0)
		db(db.auth_user.id==auth.user.id).update(timeinlevi3=0)
		db(db.auth_user.id==auth.user.id).update(currques=1)
		rows=db(db.questionslevi3.quesno==1).select()
		for row in rows:
			q=row.question
			a=row.op1
			b=row.op2
			c=row.op3
			d=row.op4
		return dict(question=q,op1=a,op2=b,op3=c,op4=d)
	else:
		return "Sorry, you haven't completed previous level"

def echoi3():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevi3
		ques=i.currques
	rows=db(db.questionslevi3.quesno==ques).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevi3=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==10 and (mark+1)>=8:
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==10 and (mark+1)<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==10 and mark>=8:
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==10 and mark<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
	
def nexti3():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevi3
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevi3.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
	if ques!=10:
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
def storei3():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	k=str(request.vars.abc)
	k=int(k)
	db(db.auth_user.id==auth.user.id).update(timeinlevi3=k)
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		scoreinlevi1a=row.scoreinlevi3
		timeinlevi1a=row.timeinlevi3
	r=db(db.ranklistlevi.authid==refid).select()
	for i in r:
		db(db.ranklistlevi.authid==refid).validate_and_update(scoreinlevi3=scoreinlevi1a,scoreinlevi1=i.scoreinlevi1,scoreinlevi2=i.scoreinlevi2,timeinlevi1=i.timeinlevi1,timeinlevi3=timeinlevi1a,timeinlevi2=i.timeinlevi2)
	return "take_end();"

def levg2():
  	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		yes=row.complevg1
	if yes==1:
		db(db.auth_user.id==auth.user.id).update(scoreinlevg2=0)
		db(db.auth_user.id==auth.user.id).update(timeinlevg2=0)
		db(db.auth_user.id==auth.user.id).update(currques=1)
		rows=db(db.questionslevg2.quesno==1).select()
		for row in rows:
			q=row.question
			a=row.op1
			b=row.op2
			c=row.op3
			d=row.op4
		return dict(question=q,op1=a,op2=b,op3=c,op4=d)
	else:
 		return "Sorry, you haven't completed level 1"

def echog2():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevg2
		ques=i.currques
	rows=db(db.questionslevg2.quesno==ques).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevg2=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==10 and (mark+1)>=8:
			db(db.auth_user.id==auth.user.id).update(complevg2=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==10 and (mark+1)<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==10 and mark>=8:
			db(db.auth_user.id==auth.user.id).update(complevg2=1)
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==10 and mark<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
	
def nextg2():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevg2
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevg2.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
	if ques!=10:
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))

def storeg2():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	k=str(request.vars.abc)
	k=int(k)
	db(db.auth_user.id==auth.user.id).update(timeinlevg2=k)
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		scoreinlevg1a=row.scoreinlevg2
		timeinlevg1a=row.timeinlevg2
	r=db(db.ranklistlevg.authid==refid).select()
	for i in r:
		db(db.ranklistlevg.authid==refid).validate_and_update(scoreinlevg2=scoreinlevg1a,scoreinlevg1=i.scoreinlevg1,scoreinlevg3=i.scoreinlevg3,timeinlevg1=i.timeinlevg1,timeinlevg2=timeinlevg1a,timeinlevg3=i.timeinlevg3)
	return "take_end();"

def levg3():
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		yes=row.complevg2
	if yes==1:
		db(db.auth_user.id==auth.user.id).update(scoreinlevg3=0)
		db(db.auth_user.id==auth.user.id).update(timeinlevg3=0)
		db(db.auth_user.id==auth.user.id).update(currques=1)
		rows=db(db.questionslevg3.quesno==1).select()
		for row in rows:
			q=row.question
			a=row.op1
			b=row.op2
			c=row.op3
			d=row.op4
		return dict(question=q,op1=a,op2=b,op3=c,op4=d)
	else:
		return "Sorry, You haven't completed previous level"

def echog3():
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		mark=i.scoreinlevg3
		ques=i.currques
	rows=db(db.questionslevg3.quesno==ques).select()
	for row in rows:
		ans=row.rightans
	c="Correct ans"
	w="Wrong ans"
	a=""
	if str(ans) == str(request.vars.op):
		db(db.auth_user.id==auth.user.id).update(scoreinlevg3=mark+1)
		retstring="Your score: " + str(mark+1)
		if ques==10 and (mark+1)>=8:
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		elif ques==10 and (mark+1)<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		else:
			return "do_change(); jQuery('#target').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(c),repr(retstring),repr(a))
		#return "jQuery('#target').html(%s);" % repr(c)
	else:
		correctans="Correct answer is option no. " + str(ans);
		retstring="Your score: " + str(mark)
		if ques==10 and mark>=8:
			response.flash=T("Congratulations,You finished the level")
			return "do_last_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		elif ques==10 and mark<8:
			response.flash=T("Sorry, you couldn't complete the level")
			return "do_last_change_replay(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
		else:
			return "do_change(); jQuery('#wtarget').html(%s);jQuery('#target1').html(%s);jQuery('#correctans').html(%s);" % (repr(w),repr(retstring),repr(correctans))
	
def nextg3():
	a=""
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
		score=i.scoreinlevg3
	db(db.auth_user.id==auth.user.id).update(currques=ques+1)
	roll=db(db.auth_user.id==auth.user.id).select()
	for i in roll:
		ques=i.currques
	rows=db(db.questionslevg3.quesno==ques).select()
	for row in rows:
		q=row.question
		option1=row.op1
		option2=row.op2
		option3=row.op3
		option4=row.op4	
	if ques!=10:
		return "do_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))
	else:
		return "do_last_original(); jQuery('#question').html(%s);jQuery('#op1').html(%s);jQuery('#op2').html(%s);jQuery('#op3').html(%s);jQuery('#op4').html(%s);jQuery('#target').html(%s);jQuery('#correctans').html(%s);jQuery('#wtarget').html(%s);" % (repr(q),repr(option1),repr(option2),repr(option3),repr(option4),repr(a),repr(a),repr(a))

def storeg3():
	#return (request.vars.abc)
	#roll=db(db.auth_user.id==auth.user.id).select()
	k=str(request.vars.abc)
	k=int(k)
	db(db.auth_user.id==auth.user.id).update(timeinlevg3=k)
	rows=db(db.auth_user.id==auth.user.id).select()
	for row in rows:
		refid=row.id
		scoreinlevg1a=row.scoreinlevg3
		timeinlevg1a=row.timeinlevg3
	r=db(db.ranklistlevg.authid==refid).select()
	for i in r:
		db(db.ranklistlevg.authid==refid).validate_and_update(scoreinlevg3=scoreinlevg1a,scoreinlevg1=i.scoreinlevg1,scoreinlevg2=i.scoreinlevg2,timeinlevg1=i.timeinlevg1,timeinlevg3=timeinlevg1a,timeinlevg2=i.timeinlevg2)
	return "take_end();"

def rashi():
	name=[]
	score=[]
	for row in db().select(
        db.auth_user.ALL, orderby=db.auth_user.totalscorelevi|db.auth_user.totaltimelevi):
	        score.append(row.totalscorelevi)
	return score

def scoreboardi():
	listname=[]
	listscore=[]
	listmin=[]
        listsec=[]
	row=db().select(db.ranklistlevi.ALL, orderby=~db.ranklistlevi.totalscore|db.ranklistlevi.totaltime)
        for i in row:
		listname.append(i.firstname +' '+ i.lastname)
		listscore.append(i.totalscore)
                listmin.append(int(i.totaltime)/60)
		p=int(i.totaltime)%60
		if p<10:
			p='0'+str(p)
                listsec.append(p)
	return dict(name=listname,score=listscore,minu=listmin,sec=listsec)

def scoreboardg():
	listname=[]
	listscore=[]
	listmin=[]
        listsec=[]
	row=db().select(db.ranklistlevg.ALL, orderby=~db.ranklistlevg.totalscore|db.ranklistlevg.totaltime)
	for i in row:
		listname.append(i.firstname +' '+ i.lastname)
		listscore.append(i.totalscore)
                listmin.append(int(i.totaltime)/60)
		p=int(i.totaltime)%60
		if p<10:
			p='0'+str(p)
                listsec.append(p)
	return dict(name=listname,score=listscore,minu=listmin,sec=listsec)


def scoreboardt():
	listname=[]
	listscore=[]
	row=db().select(db.ranklistlevt.ALL, orderby=~db.ranklistlevt.totalscore)
	for i in row:
		listname.append(i.firstname +' '+ i.lastname)
		listscore.append(i.totalscore)
          	
	return dict(name=listname,score=listscore)
